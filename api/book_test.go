package api

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBookToJSON(t *testing.T) {
	book := Book{Title: "Cloud Native Go", Author: "M.L. Reimer", ISBN: "1234567890"}
	json := book.ToJSON()
	assert.Equal(t, `{"title":"Cloud Native Go","author":"M.L. Reimer","isbn":"1234567890"}`, string(json), "Book JSON marshalling did not work.")
}

func TestBookFromJSON(t *testing.T) {
	json := []byte(`{"title":"Cloud Native Go","author":"M.L. Reimer","isbn":"1234567890"}`)
	book := FromJSON(json)

	assert.Equal(t, Book{Title: "Cloud Native Go", Author: "M.L. Reimer", ISBN: "1234567890"}, book, "Book JSON unmarshalling did not work")
}
