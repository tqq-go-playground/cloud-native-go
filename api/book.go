package api

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

// Book is a struct representing the properties of a book
type Book struct {
	Title       string `json:"title"` // The string at the end tells us how we want to unmarshal the field names
	Author      string `json:"author"`
	ISBN        string `json:"isbn"`
	Description string `json:"description,omitempty"`
}

// ToJSON converts a Book object to a JSON string
func (b Book) ToJSON() []byte {
	ToJSON, err := json.Marshal(b)

	if err != nil {
		panic(err)
	}

	return ToJSON
}

// FromJSON converts a JSON string into a Book object
func FromJSON(data []byte) Book {
	book := Book{}

	err := json.Unmarshal(data, &book)

	if err != nil {
		panic(err)
	}

	return book
}

// Books is the in-memory data store for this project
var books = map[string]Book{
	"0345391802": Book{Title: "The Hitchhiker's Guide to the Galaxy", Author: "Adams, Douglas", ISBN: "0345391802"},
	"0000000000": Book{Title: "Cloud Native Go", Author: "Reimer, M.-Leander", ISBN: "0000000000"},
}

// BooksHandleFunc is the API handler function for books
func BooksHandleFunc(w http.ResponseWriter, r *http.Request) {
	switch method := r.Method; method {
	case http.MethodGet:
		books := AllBooks()
		writeJSON(w, books)
	case http.MethodPost:
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}

		book := FromJSON(body)
		isbn, created := CreateBook(book)
		if created {
			w.Header().Add("Location", "/api/books/"+isbn)
			w.WriteHeader(http.StatusCreated)
		} else {
			w.WriteHeader(http.StatusConflict)
		}
	default:
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Unsupported request method."))
	}
}

// BookHandleFunc is the API handler function for an individual book
func BookHandleFunc(w http.ResponseWriter, r *http.Request) {
	isbn := r.URL.Path[len("/api/books/"):] // get the isbn from the URL by taking a slice from the Path string
	switch method := r.Method; method {
	case http.MethodGet:
		book, found := GetBook(isbn)
		if found {
			writeJSON(w, []Book{book})
		} else {
			w.WriteHeader(http.StatusNotFound)
		}
	case http.MethodPut:
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}

		book := FromJSON(body)
		exists := UpdateBook(isbn, book)
		if exists {
			w.WriteHeader(http.StatusOK)
		} else {
			w.WriteHeader(http.StatusNotFound)
		}
	case http.MethodDelete:
		deleted := DeleteBook(isbn)
		if deleted {
			w.WriteHeader(http.StatusOK)
		} else {
			w.WriteHeader(http.StatusNotFound)
		}
	default:
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Unsupported request."))
	}
}

// Get Book returns a book with the ISBN requested if it exists
func GetBook(isbn string) (Book, bool) {
	if b, ok := books[isbn]; ok {
		// this book already exists
		return b, true
	}

	return Book{}, false
}

// UpdateBook updates the book with the information provided if the ISBN exists
func UpdateBook(isbn string, book Book) bool {
	if _, ok := books[isbn]; ok {
		books[isbn] = book
		return true
	}

	return false
}

// DeleteBook removes the book from the in-memory data store if it exists
func DeleteBook(isbn string) bool {
	if _, ok := books[isbn]; ok {
		// book exists so delete it
		delete(books, isbn)
		return true
	}

	return false
}

// AllBooks returns all books in the in-memory database
func AllBooks() []Book {

	b := make([]Book, len(books))
	idx := 0
	for _, book := range books {
		b[idx] = book
		idx++
	}

	return b
}

func writeJSON(w http.ResponseWriter, books []Book) {
	b, err := json.Marshal(books)
	if err != nil {
		panic(err)
	}

	w.Header().Add("Content-Type", "application/json; charset=utf-8")
	w.Write(b)
}

// CreateBook adds a book to the in-memory data store if it doesn't already exist
func CreateBook(book Book) (string, bool) {
	if b, ok := books[book.ISBN]; ok {
		// this book already exists
		return b.ISBN, false
	}

	// add the book
	books[book.ISBN] = book
	return book.ISBN, true
}
